package org.academiadecodigo.wiz;

public class Main {

    public static void main(String[] args) {
        Thread trabalhador = new Thread(new TrabalhadorDasCaldas());
        trabalhador.start();
        try {
            System.out.println("Gerente: à espera que os trabaladores acabem.");
            trabalhador.join();
            System.out.println("Gerente: Finalmente acabaram, vou para casa...");
        } catch (InterruptedException e) {
            System.out.println("Gerente: acordaram-me da sesta mas ainda não acabaram...");
        }
    }
}

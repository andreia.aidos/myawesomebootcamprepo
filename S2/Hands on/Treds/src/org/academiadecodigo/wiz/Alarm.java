package org.academiadecodigo.wiz;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Alarm {

    Timer timer; //agendar tarefa para o futuro;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Number of times to rings:");
        int rings = Integer.parseInt(scanner.next());

        System.out.println("Ring interval in seconds:");
        int interval = Integer.parseInt(scanner.next());

        Alarm alarm = new Alarm();
        alarm.start(rings, interval);

        System.out.println(Thread.currentThread().getName());

    }

    private void start(int rings, int interval){
        timer = new Timer();
        timer.scheduleAtFixedRate(new Ring(rings), interval * 1000, interval * 1000);

    }

    private class Ring extends TimerTask{ //tarefa para o futuro

        int rings;

        public Ring(int rings) {
            this.rings = rings;
        }

        @Override
        public void run() {
            System.out.println("Alarm is ringing!");
            System.out.println(Thread.currentThread().getName());
            rings--;
            if (rings==0){
                System.out.println("Alarm stopped!");
                stop();
            }

        }

        private void stop(){
            timer.cancel();
        }
    }
}

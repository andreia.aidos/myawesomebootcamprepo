import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/*
 Multitheading!!! começar pelo servidor!

 accept connections,
 criar thead para responder ao pedido que recebeu

 Server Worker - nova class runnable; nested classes?
 - listen to message and reply back; vai responder a todas as pessoas que estão ligados ao servidor;
 - dá para construir e testar com o nc (nantcat) ver se as mensagens chegam lá;

 EXTRAS:
 - comandos para sair
 - user name
 - ligação privada  a uma pessoa
  */

public class Server {

    ExecutorService threadPool;
    private ServerSocket serverSocket;
    private Socket clientSocket;

    private LinkedList<ServerWorker> serverWorkers = new LinkedList<>();

    public static void main(String[] args) {
        try {
            Server server = new Server();
            server.serverListen();
        } catch (IOException e) {
            System.out.println("Server could not listen!");
        }
    }

    public Server() throws IOException {
        serverSocket = new ServerSocket(8910);
    }

    public void serverListen() {
        threadPool = Executors.newFixedThreadPool(4);
        try {
            while (!serverSocket.isClosed()) {
                clientSocket = serverSocket.accept();
                System.out.println("Connected!");
                ServerWorker worker = new ServerWorker(clientSocket);
                threadPool.submit(worker);
                serverWorkers.add(worker);
                System.out.println("Submited!");
            }
            System.out.println("Pool closed!");
            threadPool.shutdown();
        } catch (IOException e) {
            System.out.println("The server could not receive the message from the client");
        }
    }


    private class ServerWorker implements Runnable {

        //TODO: Vai te rum RUN
        private Socket clientSocket;
        private PrintWriter out;
        private BufferedReader in;


        public ServerWorker(Socket clientSocket) throws IOException {
            this.clientSocket = clientSocket;
        }

        @Override //TODO: responder a todas as pessoas que estão ligados ao servidor;
        // TODO: Fechar sockets e streams;
        // TODO: guardar info recebida dos clientes;

        public void run() {
            try {
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                while (true) {
                    String message = in.readLine();
                    System.out.println(message);
                    for (int i = 0; i < serverWorkers.size(); i++) {
                        out.println(message);
                        out.flush();
                    }
                }
            } catch (IOException e) {
                System.out.println("Could not receive or send the message!");
            }
        }
    }
}


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/*
  listen do user input - espera que o utilizador escreve algo;
  send to server

  nem class
  Cliente Worker - nested classes?
  - listen to messages from de server
  - Print server messages
   */
public class Client {
    private Socket clientSocket;
    private String message;
    private PrintWriter out;
    private ClientWorker clientWorker;

    public Client() {
        try {
            clientSocket = new Socket("localhost", 8910);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        clientWorker = new ClientWorker();
    }


    public static void main(String[] args) {
        Client client = new Client();
        while (true) {
            client.clientListen();
            client.sendMessage();
        }

    }

    public synchronized void clientListen() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write message:");
        message = scanner.nextLine();
    }

    public synchronized void sendMessage() {
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            out.println(message);

        } catch (IOException e) {
            System.out.println("Could not send my message to the server");
        }
    }

    public void printServerMessage() {
        clientWorker.receiveAndPrintServerMessages();
    }

    public class ClientWorker {

        private BufferedReader in;

        //TODO: vai ter um RUN

        public synchronized void receiveAndPrintServerMessages() {
            try {
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String serverMessage = in.readLine();
                System.out.println(serverMessage);
                in.close();
            } catch (IOException e) {
                System.out.println("Could not read server input!");
            }
        }
    }
}

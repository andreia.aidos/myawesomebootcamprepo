package org.academiadecodigo.wiz;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/*
reading and counting number of words.
finding the first word longer than n characters.
TODO: getting the longest n words.
TODO: finding the common words between two files.
 */

public class Main {
    public static void main(String[] args) {

        String filePathAlvaro = "Resources/Alvaro.txt";
        String filePathRicardo = "Resources/Ricardo.txt";

        try {
            // reading and counting number of words
            long numWords = Files.lines(Paths.get(filePathAlvaro))
                    .map(str -> str.split(" "))
                    .flatMap(Stream::of)
                    .count();
            System.out.println(numWords);

            //finding the first word longer than n characters
            String firstWord = Files.lines(Paths.get(filePathAlvaro))
                    .map(str -> str.split(" "))
                    .flatMap(Stream::of)
                    .filter(word -> word.length() > 9).findFirst().get();
            System.out.println(firstWord);

            // TODO: getting the longest n words.
          /*  Files.lines(Paths.get(filePathAlvaro))
                    .map(str -> str.split(" "))
                    .flatMap(Stream::of)
                    .collect(Collectors.groupingBy(String::length))
                    .forEach(word -> word.compareTo(word));
          */

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

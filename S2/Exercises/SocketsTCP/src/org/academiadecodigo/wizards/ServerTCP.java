package org.academiadecodigo.wizards;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTCP {

    public ServerTCP() throws IOException {
    }

    public static void main(String[] args) throws IOException {
        ServerTCP serverTCP = new ServerTCP();
        serverTCP.start();
    }

    private ServerSocket serverSocket = new ServerSocket(8080);
    private Socket clientSocket = serverSocket.accept();
    private PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
    private BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    private String message;


    public void receiveMessage() throws IOException {
        message = in.readLine();
        if (message.equals("/quit")){
        out.close();
        in.close();
        serverSocket.close();}
    }

    public void sendMessage() {
        out.println(message);
    }

    public void start() throws IOException {
        receiveMessage();
        while (!serverSocket.isClosed()){
            sendMessage();
            receiveMessage();
        }
    }
}

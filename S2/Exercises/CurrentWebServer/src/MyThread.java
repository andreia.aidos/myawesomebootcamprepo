import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MyThread implements Runnable {

    private Socket clientSocket;

    public MyThread(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        try {
            System.out.println("Server on!");
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            DataOutputStream out = new DataOutputStream((clientSocket.getOutputStream()));
            File htmlFile = new File("www/index.html");
            File imgFile = new File("www/Batata-frita.jpeg");
            FileInputStream fileInputStream = new FileInputStream(htmlFile);
            FileInputStream fileInputStream1 = new FileInputStream(imgFile);
            int numbBytesRead;
            byte[] buff = new byte[1024];
            String readLine = in.readLine();

            if (readLine.equals("GET / HTTP/1.1")) {
                int bytes = (int) Files.size(Paths.get("www/index.html"));
                System.out.println("Index");
                String str = "HTTP/1.0 200 Document Follows\r\n" +
                        "Content-Type: text/html; charset=UTF-8\r\n" +
                        "Content-Length: <" + bytes + "> \r\n" +
                        "\r\n";
                out.writeBytes(str);
                while ((numbBytesRead = fileInputStream.read(buff, 0, buff.length)) != -1) {
                    out.write(buff, 0, numbBytesRead);
                }
            }

            if (readLine.equals("GET /img HTTP/1.1")) {
                int img = (int) Files.size(Paths.get("www/Batata-frita.jpeg"));
                System.out.println("img");
                String str = "HTTP/1.0 200 Document Follows\r\n" +
                        "Content-Type: image/jpeg \r\n" +
                        "Content-Length:" + img + "\r\n" +
                        "\r\n";
                out.writeBytes(str);
                while ((numbBytesRead = fileInputStream1.read(buff, 0, buff.length)) != -1) {
                    out.write(buff, 0, numbBytesRead);
                }
            } else {
                String str = "HTTP/1.0 404 Not Found\n" +
                        "Content-Type: text/html; charset=UTF-8\r\n" +
                        "Content-Length: <file_byte_size> \r\n" +
                        "\r\n";
                out.writeBytes(str);
                out.writeBytes("404 Not Found");
                System.out.println("404");
            }

        } catch (IOException e) {
            System.out.println("Server not working");
            throw new RuntimeException(e);
        }
    }
}

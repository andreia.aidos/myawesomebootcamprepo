import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/*
O servidor está sempre à escuta
Cada vez que o servidor tem uma coneção cria um Thread novo;
Exercício de refactor
-escrever 2 ou 3 linhas de código o resto è copy paste do servidor de sexta
 */
public class Server {

    public static void main(String[] args) {
        Server server = new Server();
        try {
            server.listen();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void listen() throws IOException {
        ServerSocket serverSocket = new ServerSocket(8087);
        try {
            while (!serverSocket.isClosed()) {
                Socket clientSocket = serverSocket.accept();
                Thread thread = new Thread(new MyThread(clientSocket));
                thread.start();
            }
        } catch (IOException e) {
            System.out.println("Wrong url");
            throw new RuntimeException(e);
        }
    }
}

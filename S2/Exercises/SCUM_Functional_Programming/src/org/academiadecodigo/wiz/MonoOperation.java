package org.academiadecodigo.wiz;

public interface MonoOperation<T> {
    T monoOperation(T arg);
}

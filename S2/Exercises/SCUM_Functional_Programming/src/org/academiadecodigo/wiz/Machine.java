package org.academiadecodigo.wiz;

public class Machine<T>{

    T performMono(MonoOperation<T> machineMonoOperator, T arg){
        return machineMonoOperator.monoOperation(arg);
    }

    T biOperator(BiOperation<T> machineBiOperator, T firstArg, T secondArg){
        return machineBiOperator.biOperation(firstArg, secondArg);
    }
}

package org.academiadecodigo.wiz;

public interface BiOperation <T> {
    T biOperation(T firstArg, T secondArg);
}

package org.academiadecodigo.wiz;

public class Main {

    //strings, objetos, booleans etc.

    public static void main(String[] args) {
        Machine<String> monoMachine = new Machine<>();
        String banana = monoMachine.performMono(str -> str.toLowerCase(), "BANANA");
        String upper = monoMachine.performMono(word -> word.toUpperCase(), "sad panda");
        System.out.println("The product of my monoMachine is: " + banana + " the second is " + upper + ". Thanks for playing!");

        Machine<Boolean> monoBoolMachine = new Machine<>();
        boolean state = monoBoolMachine.performMono(bool -> false, true);
        System.out.println("My state of mind is good. This statement is: " + state + ".");

        Machine<Integer> biMachine = new Machine<>();
        int sub = biMachine.biOperator((num1, num2) -> num1-num2, 2, 1);
        int div = biMachine.biOperator((num1, num2) -> num1/num2, 4,2);
        int mul = biMachine.biOperator((num1, num2) -> num1*num2, 2, 2);
        System.out.println("The products of my biMachine are... Subtraction: " + sub + ", division: " + div + " and multiplication: " + mul + ".");
    }
}

package org.academiadecodigo;

import com.apple.eawt.AppEvent;
import sun.misc.IOUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Server {
    public Server() {
    }

    public static void main(String[] args) {
        Server server = new Server();
        server.startServer();
    }

    public void startServer() {

        try {
            ServerSocket serverSocket = new ServerSocket(8080);
            Socket clientSocket = serverSocket.accept();
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            DataOutputStream out = new DataOutputStream((clientSocket.getOutputStream()));
            File htmlFile = new File("www/index.html");
            File imgFile = new File("www/Batata-frita.jpeg");
            FileInputStream fileInputStream = new FileInputStream(htmlFile);
            FileInputStream fileInputStream1 = new FileInputStream(imgFile);
            int numbBytesRead;
            byte[] buff = new byte[1024];
            String readLine = in.readLine();

            if (readLine.equals("GET / HTTP/1.1")) {
                int bytes = (int) Files.size(Paths.get("www/index.html"));
                System.out.println("Hello");
                String str = "HTTP/1.0 200 Document Follows\r\n" +
                        "Content-Type: text/html; charset=UTF-8\r\n" +
                        "Content-Length: <" + bytes + "> \r\n" +
                        "\r\n";
                out.writeBytes(str);
                while ((numbBytesRead = fileInputStream.read(buff, 0, buff.length)) != -1) {
                    out.write(buff, 0, numbBytesRead);
                }
            }

            if (readLine.equals("GET /img HTTP/1.1")) {
                int img = (int) Files.size(Paths.get("www/Batata-frita.jpeg"));
                System.out.println("img");
                String str = "HTTP/1.0 200 Document Follows\r\n" +
                        "Content-Type: image/jpeg \r\n" +
                        "Content-Length:" + img + "\r\n" +
                        "\r\n";
                out.writeBytes(str);
                while ((numbBytesRead = fileInputStream1.read(buff, 0, buff.length)) != -1) {
                    out.write(buff, 0, numbBytesRead);
                }
            }
            
            else {
                String str = "HTTP/1.0 404 Not Found\n" +
                        "Content-Type: text/html; charset=UTF-8\r\n" +
                        "Content-Length: <file_byte_size> \r\n" +
                        "\r\n";
                out.writeBytes(str);
                out.writeBytes("404 Not Found");
            }

        } catch (IOException e) {
            System.out.println("Wrong url");
        }
    }
}
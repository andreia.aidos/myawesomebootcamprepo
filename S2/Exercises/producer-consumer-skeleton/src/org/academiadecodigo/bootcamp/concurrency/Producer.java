package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;
import org.academiadecodigo.bootcamp.concurrency.bqueue.Pizza;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Pizza> queue;
    private int elementNum;
    private int currentElementNum;

    /**
     * @param queue the blocking queue to add elements to
     * @param elementNum the number of elements to produce
     */
    public Producer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public synchronized void run() {
        for (int i = 0; i < elementNum; i++) {
            try {
                Thread.sleep((long) Math.random()*3000);
            } catch (InterruptedException e) {
                System.out.println("I don´t sleep beeaaaatch!");
            }
            queue.offer(new Pizza());
            currentElementNum++;
            System.out.println(Thread.currentThread().getName() + " has produced " + currentElementNum + " pizzas and still has " + (elementNum - currentElementNum) + " to produce!");
            System.out.println("The table size is: " + queue.getSize());
        }
        System.out.println(Thread.currentThread().getName() + " has finished producing his quota: " + elementNum + " pizzas");
    }
}

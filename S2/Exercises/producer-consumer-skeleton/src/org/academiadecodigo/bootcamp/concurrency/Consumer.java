package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;
import org.academiadecodigo.bootcamp.concurrency.bqueue.Pizza;

/**
 * Consumer of integers from a blocking queue
 */
public class Consumer implements Runnable {

    private final BQueue<Pizza> queue;
    private int elementNum;
    private int currentElementNum;

    /**
     * @param queue the blocking queue to consume elements from
     * @param elementNum the number of elements to consume
     */
    public Consumer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public synchronized void run() {
        for (int i = 0; i < elementNum; i++) {
            try {
                Thread.sleep((long) Math.random()*3000);
            } catch (InterruptedException e) {
                System.out.println("I don´t sleep beeaaaatch!");
            }
            queue.poll();
            currentElementNum++;
            System.out.println(Thread.currentThread().getName() + " has eaten " + currentElementNum + " pizzas and still has " + (elementNum-currentElementNum) + " to eat!");
            System.out.println("The table size is: " + queue.getSize());
        }
        System.out.println(Thread.currentThread().getName() + " has finished eating his daily intake: " + elementNum + " pizzas");
    }
}

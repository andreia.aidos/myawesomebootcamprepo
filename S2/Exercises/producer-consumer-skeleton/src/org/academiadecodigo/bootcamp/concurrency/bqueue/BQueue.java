package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Blocking Queue
 *
 * @param <Pizza> the type of elements stored by this queue
 */
public class BQueue<Pizza> {
    private int limit;
    private int currentPizzaNum;
    private LinkedList<Pizza> table;


    /**
     * Constructs a new queue with a maximum size
     *
     * @param limit the queue size
     */
    public BQueue(int limit) {
        this.limit = limit;
        table = new LinkedList<>();
    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     *
     * @param pizza the data to add to the queue
     * @return
     */
    public void offer(Pizza pizza) {
        synchronized (this) {
            if (currentPizzaNum < limit) {
                currentPizzaNum++;
                table.offer(pizza);
            } else {
                try {
                    System.out.println(Thread.currentThread().getName() + " Queue is  full - waiting for available space!");
                    wait();
                } catch (InterruptedException e) {
                    System.out.println("Not allowed to wait for space!");
                }
            }
            notifyAll();
        }
    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     *
     * @return the data from the head of the queue
     */
    public void poll() {
        synchronized (this) {
            if (currentPizzaNum > 0) {
                currentPizzaNum--;
                table.poll();
            } else {
                try {
                    System.out.println(Thread.currentThread().getName() + " Queue is empty - waiting for pizzas!");
                    wait();
                } catch (InterruptedException e) {
                    System.out.println("Not allowed to wait for pizzas!");
                }
            }
            notifyAll();
        }
    }


    /**
     * Gets the number of elements in the queue
     *
     * @return the number of elements
     */
    public int getSize() {
        return table.size();
    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     *
     * @return the maximum number of elements
     */
    public int getLimit() {
        return limit;
    }

    public int getCurrentPizzaNum() {
        return currentPizzaNum;
    }
}

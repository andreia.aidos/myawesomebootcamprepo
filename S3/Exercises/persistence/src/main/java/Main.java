import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {

        EntityManagerFactory entetyManagerFactory = Persistence.createEntityManagerFactory("test");
        EntityManager entityManager = entetyManagerFactory.createEntityManager();

        System.out.println("Result: " + entityManager.createNativeQuery("SELECT 80000 + 85").getSingleResult());

        entetyManagerFactory.close();
        entityManager.close();

    }
}

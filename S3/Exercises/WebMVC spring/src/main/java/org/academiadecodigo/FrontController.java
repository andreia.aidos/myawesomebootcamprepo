package org.academiadecodigo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FrontController {

    Customer customer = new Customer();

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String sayHello(Model model) {
        model.addAttribute("customer", customer);
        return "index";
    }
}

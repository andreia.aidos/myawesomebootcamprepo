package org.academiadecodigo.wizards;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        FileCopy fileCopy = new FileCopy();

        try {
            fileCopy.copy("resources/File.pdf", "resources/Copy.pdf");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

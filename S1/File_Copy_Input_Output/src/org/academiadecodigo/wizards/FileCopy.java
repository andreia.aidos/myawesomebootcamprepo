package org.academiadecodigo.wizards;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopy {
    byte[] buffer = new byte[1024];

    int num;

    public void copy(String sourceFile, String copiedFile) throws FileNotFoundException, IOException {

        FileInputStream inputStream = new FileInputStream(sourceFile);
        FileOutputStream outputStream = new FileOutputStream(copiedFile);

        while (num != -1) {
            num = inputStream.read(buffer);
            System.out.println(num);
            if (num != -1) {
                outputStream.write(buffer, 0, num);
            }
        }
        inputStream.close();
        outputStream.close();
    }
}

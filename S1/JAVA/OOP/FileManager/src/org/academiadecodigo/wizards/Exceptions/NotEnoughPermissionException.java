package org.academiadecodigo.wizards.Exceptions;

public class NotEnoughPermissionException extends FileException {
    public NotEnoughPermissionException(){
        super("You are not logged in!");
    }
}

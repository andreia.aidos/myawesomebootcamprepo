package org.academiadecodigo.wizards.Exceptions;

public class FileException extends Exception {

    public FileException(String message) {
        super(message);
    }
}

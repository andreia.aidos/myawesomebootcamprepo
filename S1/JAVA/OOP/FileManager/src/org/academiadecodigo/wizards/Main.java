package org.academiadecodigo.wizards;

import org.academiadecodigo.wizards.Exceptions.FileNotFoundException;
import org.academiadecodigo.wizards.Exceptions.NotEnoughSpaceException;
import org.academiadecodigo.wizards.Exceptions.NotEnoughPermissionException;

public class Main {
    public static void main(String[] args) {

        FileManager fileManager = new FileManager(7);

        try {
            fileManager.login();
            fileManager.hasMemorySpace();
            fileManager.makeNewFile("firstFile");
        } catch (NotEnoughPermissionException e) {
            System.out.println(e.getMessage());
        } catch (NotEnoughSpaceException e) {
            System.out.println(e.getMessage());
        }

        try {
            fileManager.hasFile(3);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }
}

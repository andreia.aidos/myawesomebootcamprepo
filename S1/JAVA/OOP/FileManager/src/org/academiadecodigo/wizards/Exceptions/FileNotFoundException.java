package org.academiadecodigo.wizards.Exceptions;

public class FileNotFoundException extends FileException{

    public FileNotFoundException() {
        super("File not found, BEACH!");
    }
}

package org.academiadecodigo.wizards.Exceptions;

public class NotEnoughSpaceException extends FileException {


    public NotEnoughSpaceException() {
        super("You don't have space.");
    }

}

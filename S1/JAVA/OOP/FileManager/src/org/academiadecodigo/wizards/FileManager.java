package org.academiadecodigo.wizards;

import org.academiadecodigo.wizards.Exceptions.FileNotFoundException;
import org.academiadecodigo.wizards.Exceptions.NotEnoughSpaceException;
import org.academiadecodigo.wizards.Exceptions.NotEnoughPermissionException;

public class FileManager {

    private boolean loggedIn;
    private final File[] files;

    public FileManager(int numbFiles) {
        this.files = new File[numbFiles];
    }

    public void makeNewFile(String fileName) throws NotEnoughPermissionException, NotEnoughSpaceException {

        if (!loggedIn) {
            throw new NotEnoughPermissionException();
        }

        if (!hasMemorySpace()) {
            throw new NotEnoughSpaceException();
        }

        for (int i = 0; i < files.length; i++) {
            if (files[i] == null) {
                files[i] = new File(fileName);
            }
        }
    }

    public void login() {
        loggedIn = true;
    }

    public boolean hasMemorySpace() {
        for (File file : files) {
            if (file == null) {
                return true;
            }
        }
        return false;
    }

    public void hasFile(int index) throws FileNotFoundException {
        boolean hasFileInIndex;
        if (files[index] == null) {
            throw new FileNotFoundException();
        }
        hasFileInIndex = true;
    }

}

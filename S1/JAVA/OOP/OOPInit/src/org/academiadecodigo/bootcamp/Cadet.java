package org.academiadecodigo.bootcamp;

// I have Cadets with different levels of (0 - 10), every time a cadet learns something
// the cadet will increase a point, he will decrease a point if he arrives to class late

public class Cadet {

    private String name;
    public int level;
    public String hobbie;

    //Encapsulation
    public Cadet(String name, int level) {
        this.name = name;
        this.level = level;
    }
    //Now i want to allow the client to be able to invoque the name and level besides it being
    // private

    public String getName() {
        return this.name;
    }

    //set is used so i may change the property valiu, for exemple if i have the property
    // name private i may change it by invoquing the method setName()

    public void setName(String name) {
        this.name = name;
    }

    //se o primiro Cadet has a greater level it will print "the Cadet 1 lost a point and know
    // has the new point
    public void addToCadetLevel() {
        this.level++;
        System.out.println("The " + this.getName() + " gained a point and know has " + this.level + ".");
    }

    public void subtractToCadetLevel() {
        this.level--;
        System.out.println("The " + this.getName() + " lost a point and know has " + this.level + ".");
    }

    public void smartnessLevel() {
        if (this.level < 5) {
            System.out.println(this.getName() + " your knowledge is low!");
        } else if (this.level > 5) {
            System.out.println(this.getName() + " your knowledge is average!");
        } else {
            System.out.println(this.getName() + " your knowledge is high!");
        }
    }
}
package org.academiadecodigo.bootcamp;


public class Main {
    public static void main(String[] args) {
        //Construtors
        Cadet iAmPear = new Cadet("Bernardo", 10);
        Cadet boob = new Cadet("Fabio", 9);
        Cadet aidos = new Cadet("Andreia", 8);
        Cadet afonso = new Cadet("Afonso", 7);
        Cadet carlao = new Cadet("Carlos", 6);
        Cadet choco = new Cadet("Renato", 5);

        System.out.println("The cadet " + iAmPear.getName() + " has a level of " + iAmPear.level + ".");
        // iAmPear learned something new and got awarded a point
        iAmPear.addToCadetLevel();
        // iAmPear lost a point because he got late to class
        iAmPear.subtractToCadetLevel();
        // choco wants to know the level of his knowledge
        choco.smartnessLevel();
        // a new rule was added now you may give a point to a cadet you think deserves it but
        // it will decrease of your
        System.out.println("Because " + carlao.getName() + " thinks he deserves e gave a point to " + afonso.getName() + ". Now the new level of " + afonso.getName() + " is " + (afonso.level - 1) + " and " + carlão.getName() + " new level is " + (carlão.level - 1) + " .");
        // boob now identifies as "Amélia"
        boob.setName("Amélia");
        System.out.println("Fábio now identifies as " + boob.getName());
        //aidos shared that she has a hobbies
        aidos.hobbie = "Watching movies!";
    }
}

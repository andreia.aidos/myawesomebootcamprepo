package org.academiadecodigo.wizards;
/*
DONE: When released from the lamp (instantiated), the maximum number of wishes is set
DONE: It can grant wishes if the maximum has not been reached
 */
public class Friendly extends Genie {

    public Friendly(int numberOfWishes, int grantedWishes) {
        super(numberOfWishes, grantedWishes);
    }

    @Override
    public void grantWishes() {
        if (getGrantedWishes() <= getNumberOfWishes() && getGrantedWishes() > 0) {
            super.grantWishes();
        } else {
            System.out.println("This Friendly genie has no more wishes to grant!");
        }
    }
}

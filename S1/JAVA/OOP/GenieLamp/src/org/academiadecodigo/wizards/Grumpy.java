package org.academiadecodigo.wizards;
/*
DONE: When released from the lamp (instantiated), the maximum number of wishes is set
DONE: Grants ONLY ONE wish, even if the maximum has not been reached;
 */
public class Grumpy extends Genie {

    public Grumpy(int numberOfWishes, int grantedWishes) {
        super(numberOfWishes, grantedWishes);
    }

    @Override
    public void grantWishes() {
        if (getGrantedWishes() == 1) {
            super.grantWishes();
        } else if (getGrantedWishes() == 0) {
            System.out.println("This Grumpy genie has no more wishes to grant!");
        }
    }
}

package org.academiadecodigo.wizards;
/*
Done: Releases a new genie every time it is rubbed
Done: When enchanted (instantiated), the maximum number of genies is set
Done: Genies can be Friendly (even) or Grumpy (odd)
Done: When the number of genies is exhausted, it releases a recyclable demon
TODO: ?????Has the ability to recharge itself by recycling a demon?????
EXTRA EXTRA: We can compare two lamps by their capacity, number of remaining genies and number of times it has been recharged
 */
public class MagicLamp {

    private int numberOfGenies;

    public MagicLamp(int numberOfGenies) {
        this.numberOfGenies = numberOfGenies;
    }

    public int getNumberOfGenies() {
        return numberOfGenies;
    }

    public void lampContent() {
        System.out.println("The magic lamp has " + numberOfGenies + " genies.");
    }
    //TODO: try a method to call all rubs??
   // public Genie rub(){
     //   return rubOneTime()*(numberOfGenies+1);
   // }
    //TODO: find a way not to have parameters
    public Genie rubOneTime(int number, int numberOfWishes, int grantedWishes) {
        if (numberOfGenies > 0) {
            if (number % 2 == 0) {
                System.out.println("A Friendly genie is free!");
                numberOfGenies--;
                lampContent();
                return new Friendly(numberOfWishes, grantedWishes);
            } else {
                System.out.println("A Grumpy genie is free!");
                numberOfGenies--;
                lampContent();
                return new Grumpy(numberOfWishes, grantedWishes);
            }
        }
        System.out.println("All genies are free! The Recyclable Demon is released!");
        lampContent();
        return new RecyclableDemon(numberOfWishes, grantedWishes);
    }

    //TODO: ????method recycle - it will setRechargedTheLamp to true and we will make a condition thant doesn't allow access to teh wishes;

}


package org.academiadecodigo.wizards;
//TODO: method grant only one wish at a time and only after all other genies have granted a wish
public class Genie {
    private int numberOfWishes;
    private int grantedWishes;

    public Genie(int numberOfWishes, int grantedWishes) {
       this.numberOfWishes = numberOfWishes;
       this.grantedWishes = grantedWishes;
    }

     public void grantWishes() {
         System.out.println("Wish has been granted my Master");
         grantedWishes--;
     }

    public int getNumberOfWishes() {
        return numberOfWishes;
    }

    public void setNumberOfWishes(int numberOfWishes) {
        this.numberOfWishes = numberOfWishes;
    }

    public int getGrantedWishes() {
        return grantedWishes;
    }
    public void noGenies(){
        System.out.println("There are no more genies!");
    }

    public void setGrantedWishes(int grantedWishes) {
        this.grantedWishes = grantedWishes;
    }



}

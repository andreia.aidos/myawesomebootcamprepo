package org.academiadecodigo.wizards;
/*
DONE: When released from the lamp (instantiated), the maximum number of wishes is set
DONE: Grants ALL wishes, even if the maximum has already been reached
TODO: ???It can recharge a magic lamp, if recycled
TODO: ???It will not grant any more wishes after being recycled
TODO: ???It can only be recycled once
 */
public class RecyclableDemon extends Genie {

    //private boolean rechargedTheLamp = false;

    public RecyclableDemon(int numberOfWishes, int grantedWishes/*, boolean rechargedTheLamp*/) {
        super(numberOfWishes, grantedWishes);
        // this.rechargedTheLamp = rechargedTheLamp;
    }

    @Override
    public void grantWishes() {
        if (getGrantedWishes() > 0) {
            System.out.println("Wish has been granted my Master");
        }

        //public void setRechargedTheLamp(boolean rechargedTheLamp) {
        //  this.rechargedTheLamp = rechargedTheLamp;
        //}
    }
}

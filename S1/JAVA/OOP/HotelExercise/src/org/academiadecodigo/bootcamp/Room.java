package org.academiadecodigo.bootcamp;

public class Room {

    private boolean availability;

    public Room(boolean availability) {
        this.availability = availability;
    }

    public boolean getAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }
}

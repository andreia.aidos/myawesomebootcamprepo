package org.academiadecodigo.bootcamp;

/*
Create a hotel (or more)
- guestes
- rooms
- hotel
- methods:
	. check in - free rooms or not
Not be able to do check in 2 times, condition to the availability of the rooms
	. check out
Not able to check out if you don’t do check in,

Extras:
- cleaning service
- catering service
- reception
 */

public class Main {
    public static void main(String[] args) {

        Hotel hotel1 = new Hotel("Paradise");
        Hotel hotel2 = new Hotel("Montebelo Ilhavo");
        Guest guest1 = new Guest("Consuela", false);
        Guest guest2 = new Guest("António", false);
        Guest guest3 = new Guest("Amélia", false);

        guest1.checkIn(hotel1);
        guest2.checkIn(hotel1);
        guest1.checkOut(guest1.getRoomNumber(), hotel1);
        guest3.checkIn(hotel1);
        guest1.checkIn(hotel2);
        guest2.checkIn(hotel2);




    }
}

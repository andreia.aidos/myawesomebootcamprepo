package org.academiadecodigo.bootcamp;

public class Guest {

    private String name;
    private boolean hasRoom;

    private int roomNumber;

    public Guest(String name, Boolean hasRoom) {
        this.name = name;
        this.hasRoom = hasRoom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getHasRoom() {
        return hasRoom;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setHasRoom(Boolean hasRoom) {
        this.hasRoom = hasRoom;
    }

    public void checkIn(Hotel hotel) {

        if (hasRoom) {
            System.out.println("Sorry, i already have you in me system. Are you sure you didn't do the check in already?");

        } else if (hotel.checkAllRoomsAvailability() >= 0 && hotel.checkAllRoomsAvailability() <= 10) {
            int roomNumberAndIndex = hotel.checkAllRoomsAvailability();
            System.out.println("Dear guest " + name + ", we have a free room! Your room number is " + roomNumberAndIndex + ". Welcome to " + hotel.getName() + ".");
            hasRoom = true;
            hotel.changeRoomAvailability(roomNumberAndIndex, false);
            roomNumber = roomNumberAndIndex;

        } else if (hotel.checkAllRoomsAvailability() > 10) { //room[].length()??
            System.out.println("We're so sorry but we don't have free rooms!");

        } else {
            System.out.println("We're so sorry but we don't have free rooms!");
        }
    }

    public void checkOut(int roomNumb, Hotel hotel) {
        if (hasRoom) {
            System.out.println("Dear " + name + " thank you for saying with us, hope you had a lovely stay!");
            hasRoom = false;
            hotel.changeRoomAvailability(roomNumb, true);
        } else {
            System.out.println("Sorry, i can't find you in my system. Are you sure you checked in to " + hotel.getName() + ".");
        }
    }

}

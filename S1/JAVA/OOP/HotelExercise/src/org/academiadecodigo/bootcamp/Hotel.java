package org.academiadecodigo.bootcamp;

import java.lang.reflect.Array;

public class Hotel {

    private Room[] rooms = new Room[10];

    private String name;

    public Hotel(String name) {
        this.name = name;
        for (int i = 0; i < rooms.length; i++) {
            rooms[i] = new Room(true);
        }
    }

    public String getName() {
        return name;
    }

    public Room[] getRooms() {
        return rooms;
    }


    public void changeRoomAvailability(int index, boolean availability) {
        rooms[index].setAvailability(availability);
    }

    public boolean checkRoomAvailability(int index) {
        return rooms[index].getAvailability();
    }

    public int checkAllRoomsAvailability() {

        for (int i = 0; i < rooms.length; i++) {
                if (checkRoomAvailability(i)) {
                    return i;
                }

        }
        return -1;
    }
}
//method to find available rooms Return the available room to tell the guest to choose



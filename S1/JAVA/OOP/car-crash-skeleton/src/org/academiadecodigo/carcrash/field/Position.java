package org.academiadecodigo.carcrash.field;

import org.academiadecodigo.carcrash.cars.Fiat;

//Describes where a Car lives on the Grid
//Updating position results in moving the Car
public class Position {

    private int col;
    private int row;

    public Position() {
        col = (int) Math.floor(Math.random() * Field.getWidth());
        row = (int) Math.floor(Math.random() * Field.getHeight());

    }

    public int getCol() {
        return col;
    }

    public int getRow() {

        return row;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void moveRight() {
        if (col < Field.getWidth()-1) {
            col++;
        }
    }

    public void moveLeft() {
        if (col > 0) {
            col--;
        }
    }

    public void moveUp() {
        if (row > 0) {
            row--;
        }
    }

    public void moveDown() {
        if (row < 24) {
            row++;
        }
    }


}

package org.academiadecodigo.carcrash.cars;
//DONE:Different types of cars exist
//Cars move at different speeds and in different ways
//A car can crash at any time

import org.academiadecodigo.carcrash.field.Position;

abstract public class Car {

    /**
     * The position of the car on the grid
     */
    private Position pos;

    private boolean moving = true;

    public Car() {
        pos = new Position();
    }

    public Position getPos() {
        return pos;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public boolean getMoving() {
        return moving;
    }

    public boolean isCrashed() {
        return false;
    }


}

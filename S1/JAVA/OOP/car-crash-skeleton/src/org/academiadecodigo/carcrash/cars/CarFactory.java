package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

//DONE: Creates cars
public class CarFactory {

    public static Car getNewCar() {
        if (CarType.values()[(int) Math.floor(Math.random() * CarType.values().length)] == CarType.FIAT) {
            return new Fiat();
        }
        return new Mustang();
    }
}


package org.academiadecodigo.carcrash;

//TODO: Makes all the cars move (method end of the page);
//Checks for collisions between cars

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.cars.CarFactory;
import org.academiadecodigo.carcrash.cars.Fiat;
import org.academiadecodigo.carcrash.field.Field;

import java.util.Scanner;

public class Game {

    public static final int MANUFACTURED_CARS = 20;

    /**
     * Container of Cars
     */
    private Car[] cars;

    /**
     * Animation delay
     */
    private int delay;

    public Game(int cols, int rows, int delay) {

        Field.init(cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

        cars = new Car[MANUFACTURED_CARS];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = CarFactory.getNewCar();
        }

        Field.draw(cars);

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);

            // Move all cars
            moveAllCars();

            // Update screen
            Field.draw(cars);
        }
    }


    private void moveAllCars() {

        for (int i = 0; i < cars.length; i++) {
            for (int j = 1; j < cars.length; j++) {
                if (cars[i].getPos() == cars[j].getPos()) {
                    cars[i].setMoving(false);
                    cars[j].setMoving(false);
                }
            }
        }
        for (int i = 0; i < cars.length; i++) {

            int move = (int) Math.floor(Math.random() * 4);
            switch (move) {
                case 0:
                    if (cars[i].getMoving() == false) {
                        cars[i].getPos().moveUp();
                    }
                    break;
                case 1:
                    if (cars[i].getMoving() == false) {
                        cars[i].getPos().moveDown();
                    }
                    break;
                case 2:
                    if (cars[i].getMoving() == false) {
                        cars[i].getPos().moveRight();
                    }
                    break;
                case 3:
                    if (cars[i].getMoving() == false) {
                        cars[i].getPos().moveLeft();
                    }
                    break;
            }

        }
    }
}



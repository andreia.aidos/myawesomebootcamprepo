package org.academiadecodigo.bootcamp.containers;

public class LinkedList <T>{

    private Node head;
    private int length = 0;

    public LinkedList() {
        this.head = new Node(null);
    }

    public int size() {
        return length;
    }

    /**
     * Adds an element to the end of the list
     *
     * @param data the element to add
     */
    public void add(T data) {

        Node node = new Node(data);
        Node iterator = head;
        while (iterator.getNext() != null) {
            iterator = iterator.getNext();
        }
        iterator.setNext(node);
        length++;

    }

    /**
     * Obtains an element by index
     *
     * @param index the index of the element
     * @return the element
     */
    public Object get(int index) {
        Node iterator = head;
        int i = -1;
        while (i < index) {
            if (iterator.getNext() == null) {
                return null;
            }
            iterator = iterator.getNext();
            i++;
        }
        return iterator.getData();
    }


    /**
     * Returns the index of the element in the list
     *
     * @param data element to search for
     * @return the index of the element, or -1 if the list does not contain element
     */
    public int indexOf(Object data) {
        int index = 0;
        Node iterator = head.getNext();
        while (iterator != null) {
            if (!iterator.getData().equals(data)) {
                index++;
                iterator = iterator.getNext();
                continue;
            }
            return index;
        }
        return -1;
    }

    /**
     * Removes an element from the list
     *
     * @param data the element to remove
     * @return true if element was removed
     */
    public boolean remove(T data) {

        Node iterator = head;
        if (iterator.getNext() == null) {
            return false;
        }
        while (!iterator.getNext().getData().equals(data)) {
            iterator = iterator.getNext();
            if (iterator.getNext() == null) {
                return false;
            }
        }
        iterator.setNext(iterator.getNext().getNext());
        length--;
        return true;
    }

    private class Node <T>{

        private T data;
        private Node next;

        public Node(T data) {
            this.data = data;
            next = null;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }
}



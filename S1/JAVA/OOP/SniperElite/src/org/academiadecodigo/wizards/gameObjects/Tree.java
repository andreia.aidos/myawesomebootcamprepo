package org.academiadecodigo.wizards.gameObjects;

import org.academiadecodigo.wizards.gameObjects.GameObject;

public class Tree extends GameObject {

    @Override
    public String getMessage(){
        return "I am a tree!";
    }
}

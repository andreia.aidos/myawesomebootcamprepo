package org.academiadecodigo.wizards;

import org.academiadecodigo.wizards.gameObjects.GameObject;

public class Main {
    public static void main(String[] args) {

        Game game1 = new Game(10);
        SniperRifle rifle1 = new SniperRifle();


        for(GameObject gameObject : game1.getGameObjects()) {
           System.out.println(gameObject.getMessage());

        }
        System.out.println("---------------------------");
        game1.start(rifle1);
    }
}

package org.academiadecodigo.wizards.gameObjects;

import org.academiadecodigo.wizards.gameObjects.Enemy;

/*DONE: ? health-- method
Soldier: it loses health on each hit;

 */
public class SoldierEnemy extends Enemy {

    @Override
    public void hit(int bulletDamage) {
        System.out.println(getMessage() + "was hit!");
        super.hit(bulletDamage);
        System.out.println("********");
    }

    @Override
    public String getMessage() {
        return "Enemy Soldier ";
    }
}

package org.academiadecodigo.wizards.gameObjects;

import org.academiadecodigo.wizards.gameObjects.Enemy;

/*
DONE: if the armour has points it takes the damage, else the enemy loses health.
*/
public class ArmouredEnemy extends Enemy {

    private int armour = 2;


    @Override
    public void hit(int bulletDamage) {
        System.out.println(getMessage() + "was hit!");
        if (armour > 0) {
            armour--;
            System.out.println("You still have armour points: " + armour);
        }
        if (armour == 0) {
            super.hit(bulletDamage);
        }
        System.out.println("********");

    }

    @Override
    public String getMessage() {
        return "Armoured Enemy ";
    }

}


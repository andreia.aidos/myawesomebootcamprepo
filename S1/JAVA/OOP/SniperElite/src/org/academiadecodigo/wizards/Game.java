package org.academiadecodigo.wizards;

import org.academiadecodigo.wizards.gameObjects.*;

/* DONE: When creating the game specify the number of game objects;
 DONE: Create an array of game objects with enemies and trees;
DONE: Create an if condition that will limit the probability of having tree objects;
DONE: ? create a for loop with a switch as an argument;
Iterate through the array and shoot only the enemies;
DONE: ? edge case do not shot the trees;
Do NOT shoot the Trees!;
DONE: wile enemy health > then 0 keep shooting util he is dead;
DONE: return the number of shots;
Keep shooting the same enemy until it is dead;
At the end show the total number of shots fired.
*/
public class Game {

    private GameObject[] gameObjects;
    private int numberObjects;
    private SniperRifle sniperRifle;
    private int shotsFired;

    public Game(int numberObjects) {
        this.numberObjects = numberObjects;
        this.gameObjects = new GameObject[numberObjects];
        createObjects();
    }

    public void start(SniperRifle rifle) {

        for (int i = 0; i < gameObjects.length; i++) {
            if (gameObjects[i] instanceof Tree) {
                System.out.println("Don't shoot that's a tree!");
                continue;
            }
            rifle.shoot((Enemy) gameObjects[i]);
        }

    }


    private GameObject[] createObjects() {
        for (int i = 0; i < gameObjects.length; i++) {
            int random = (int) (Math.floor(Math.random() * 3));
            switch (random) {
                case 0:
                    gameObjects[i] = new ArmouredEnemy();
                    break;

                case 1:
                    gameObjects[i] = new SoldierEnemy();
                    break;

                default:
                    gameObjects[i] = new Tree();
                    break;
            }
        }
        return gameObjects;
    }

    public GameObject[] getGameObjects() {
        return gameObjects;
    }
}

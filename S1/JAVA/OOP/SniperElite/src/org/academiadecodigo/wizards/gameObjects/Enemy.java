package org.academiadecodigo.wizards.gameObjects;


public abstract class Enemy extends GameObject {

    private int health = 2;
    private boolean isDead = false;

    //The method isDead will change the value of isDead to true if health ==0, else it will return false;
    public boolean isDead() {
        if (health == 0) {
            isDead = true;
        }
        return isDead;
    }

    public boolean getIsDead() {
        return isDead;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    //TODO: why int? in the method hit()?
    public void hit(int bulletDamage) {
        if (health > 0) {
            health = health - bulletDamage;
            System.out.println("You were hit, your health is " + health);
        }
        if (health == 0) {
            isDead = true;
            System.out.println("You are Dead!");
        }

    }

}

package org.academiadecodigo.wizards;

import org.academiadecodigo.wizards.gameObjects.Enemy;

/* When shooting, it has a probability of hitting the target with a certain damage amount.
DONE: Create an if condition with Math.random() of two possibility. Miss or hit.
DONE: When it hits it will create damage in the form of health-- && armour--;
 */
public class SniperRifle {

    private int bulletDamage = 1;
    //private int numberOfShoots;

    public void shoot(Enemy enemy) {
        while (enemy.getHealth() > 0) {
            int probabilityOfHit = (int) (Math.floor(Math.random() * 11));
            if (probabilityOfHit > 3) {
                enemy.hit(bulletDamage);
                //numberOfShoots++;
                //System.out.println("SHOT " + numberOfShoots);
            }
            if (probabilityOfHit <= 3) {
                System.out.println("You missed the target!");
               // numberOfShoots++;
                //System.out.println("SHOT" + numberOfShoots);
            }
        }
    }
}

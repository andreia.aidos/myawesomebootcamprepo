//Given a string, print a new string where "not " has been added to the front. However, if the string already begins with "not", print the string unchanged.

class EXERCISEstringNotString{

public static void main(String[] args) {

    String word = "semicolon"; // what if the word is "nothing"?

    //receive command line argument if available
    if(args.length > 0){
      word = args[0];
    }

    // print result here
	String not = "not";
   System.out.println(word.substring(0, 3) == not ? word : "not "+word);

}
}

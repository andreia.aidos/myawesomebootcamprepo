//Given a string, print a new string where the first and last characters have been exchanged.

class EXERCISEstringFrontBack{

public static void main(String[] args) {

    String word = "heisenberg";

    //receive command line argument if available
    if(args.length > 0){
      word = args[0];
    }

    //print result here
    	int wordLength = word.length()-1;
	System.out.println(word.charAt(wordLength) + word.substring(1, (wordLength-1)) + word.charAt(0));
}
}



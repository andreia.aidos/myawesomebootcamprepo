package org.academiadecodigo.wizard;


import java.util.Iterator;

public class Range<Integer> implements Iterable<Integer> {

    private Integer min;

    private Integer max;

    private boolean remove;

    public Range(Integer min, Integer max) {
        this.min = min;
        this.max = max;

        //remove.
    }


    @Override
    public Iterator<Integer> iterator() {
        return new MyIterator<>();
    }

    //  public void remove() {


    private class MyIterator<Integer> implements Iterator<Integer> {

        private Integer current = (Integer) min;

        @Override
        public boolean hasNext() {
            return (int) current < (int) max;
        }

        @Override
        public Integer next() {
            if (current > max || current < min ) {
            }
            return current++;
        }
    }
}

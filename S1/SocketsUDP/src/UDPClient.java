import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class UDPClient {

    public UDPClient() throws SocketException, UnknownHostException {
    }

    public static void main(String[] args) throws IOException {
        UDPClient client = new UDPClient();
        client.start();
    }

    private final InetAddress IP = InetAddress.getByName("localhost");
    private String message;
    private final int SERVER_PORT_NUMBER = 8080;
    private DatagramSocket socket = new DatagramSocket(8888);
    private Scanner scanner = new Scanner(System.in);
    private byte[] sendBuffer = new byte[1024];
    private byte[] receiveBuffer = new byte[1024];

    public void getUserInput() {
        System.out.println("Write something:");
        message = scanner.nextLine();
    }

    public void sendPacket() throws IOException {
        sendBuffer = message.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, IP, SERVER_PORT_NUMBER);
        socket.send(sendPacket);
    }

    public void receiveResponse() throws IOException {
        DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
        socket.receive(receivePacket);
        message = new String(receiveBuffer);
    }

    public void start() throws IOException {
        getUserInput();
        sendPacket();
        receiveResponse();
        System.out.println(message);
    }
}


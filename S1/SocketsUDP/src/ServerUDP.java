import java.io.*;
import java.net.*;

public class ServerUDP {

    public ServerUDP() throws SocketException {
    }

    public static void main(String[] args) throws IOException {
        ServerUDP udp = new ServerUDP();
        udp.start();
    }

    private InetAddress IP;
    private final int SERVER_PORT_NUMBER = 8080;
    private int clientPortNumber;
    private byte[] sendBuffer = new byte[1024];
    private byte[] receiveBuffer = new byte[1024];
    public DatagramSocket socket = new DatagramSocket(SERVER_PORT_NUMBER);

    public void getRequest() throws IOException {
        DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
        socket.receive(receivePacket);
        IP = receivePacket.getAddress();
        clientPortNumber = receivePacket.getPort();
        receiveBuffer = receivePacket.getData();
    }

    public void sendResponse() throws IOException {
        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, IP, clientPortNumber);
        socket.send(sendPacket);
    }

    public void start() throws IOException {
        getRequest();
        String message = new String(receiveBuffer);
        message = message.toUpperCase();
        sendBuffer = message.getBytes();
        sendResponse();
        socket.close();
    }
}





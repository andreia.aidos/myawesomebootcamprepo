package org.academiadecodigo.wizards;
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClientTCP {

    public static void main(String[] args) throws IOException {
        ClientTCP clientTCP = new ClientTCP();
        clientTCP.start();
    }

    private final Socket clientSocket;
    private final PrintWriter out;
    private final BufferedReader in;
    private final Scanner scanner;
    private String message;


    public ClientTCP() throws IOException {
        clientSocket = new Socket("localhost", 8080);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        scanner = new Scanner(System.in);
    }


    public void sendMessage() throws IOException {
        System.out.println("Type your message:");
        message = scanner.nextLine();
        out.println(message);
        if (message.equals("/quit")) {
            out.close();
            in.close();
            clientSocket.close();
        }
    }

    public void receiveMessage() throws IOException {
        if (message.equals("/red")) {
            System.out.println("\033[0;31m" + in.readLine());
        } else {
            System.out.println(in.readLine());
        }
    }

    public void start() throws IOException {
        sendMessage();
        while (!clientSocket.isClosed()) {
            receiveMessage();
            sendMessage();
        }
    }
}

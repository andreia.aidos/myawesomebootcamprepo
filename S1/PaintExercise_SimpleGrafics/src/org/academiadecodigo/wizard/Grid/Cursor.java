package org.academiadecodigo.wizard.Grid;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.ArrayList;

public class Cursor {

    private Grid grid;
    private Rectangle cursor;

    private Position wizPos;

    public Cursor() {

    }

    public void draw() {
        cursor.draw();
        cursor.fill();
    }

    private ArrayList<Rectangle> paintedRectangle = new ArrayList<>();

    void setRectangle(Grid x) {
        grid = x;
        cursor = new Rectangle(grid.PADDING, grid.PADDING, grid.getCellSize(), grid.getCellSize());
    }

    public void moveRight() {
        if (cursor.getX() < grid.getWidth() - grid.getCellSize()) {
            cursor.translate(grid.getCellSize(), 0);
        }
    }

    public void moveLeft() {
        if (cursor.getX() > grid.PADDING) {
            cursor.translate(-grid.getCellSize(), 0);
        }
    }

    public void moveUp() {
        if (cursor.getY() > grid.PADDING) {
            cursor.translate(0, -grid.getCellSize());
        }
    }

    public void moveDown() {
        if (cursor.getY() < grid.getHeight() - grid.getCellSize()) {
            cursor.translate(0, grid.getCellSize());
        }
    }

    public void paint(){
        Rectangle newRect = new Rectangle(cursor.getX(), cursor.getY(), grid.getCellSize(), grid.getCellSize());
        newRect.fill();
        paintedRectangle.add(newRect);
    }

    public void removeAll(){
        System.out.println(paintedRectangle.isEmpty());
        for (Rectangle x: paintedRectangle) {
            x = null;
        }

        paintedRectangle.clear();
        System.out.println("remove");
    }
    public void clearOne(){
        Rectangle newRect = new Rectangle((cursor.getX()+1), (cursor.getY()+1), grid.getCellSize()-1, grid.getCellSize()-1);
        newRect.fill();
        newRect.setColor(Color.WHITE);
    }
    public void setGrid(Grid grid) {
        this.grid = grid;
    }
}

package org.academiadecodigo.wizard.Grid;

public class Position {

    private int X;
    private int Y;
    private int width;
    private int hight;

    public Position(int x, int y, int width, int hight) {
        X = x;
        Y = y;
        this.width = width;
        this.hight = hight;
    }

    public int getX() {
        return X;
    }

    public int getY() {
        return Y;
    }

    public int getWidth() {
        return width;
    }

    public int getHight() {
        return hight;
    }

    public void setX(int x) {
        X = x;
    }

    public void setY(int y) {
        Y = y;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }
}

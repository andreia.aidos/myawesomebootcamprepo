package org.academiadecodigo.wizard.Grid.grid;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.wizard.Grid.Position;

public class Cell {

    private Position cellPos;
    private Color cellColor;
    private Rectangle cellRectangle;

    public Cell(int x, int y, int w, int h) {
        cellPos = new Position(x,y,w,h);
        cellRectangle = new Rectangle(cellPos.getX(), cellPos.getY(), cellPos.getWidth(), cellPos.getHight());
        cellRectangle.draw();
    }
}

package org.academiadecodigo.wizard.Grid.grid;

/**
 * The available grid colors
 */
public enum GridColor {
    RED,
    GREEN,
    BLUE,
    MAGENTA,
    NOCOLOR

}

package org.academiadecodigo.wizard.Grid.grid.simplegfx;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.wizard.Grid.Grid;
import org.academiadecodigo.wizard.Grid.grid.GridColor;
import org.academiadecodigo.wizard.Grid.grid.GridDirection;
import org.academiadecodigo.wizard.Grid.grid.position.AbstractGridPosition;
import org.academiadecodigo.wizard.Grid.grid.position.GridPosition;


/**
 * Simple graphics position
 */
public class SimpleGfxGridPosition extends AbstractGridPosition {

    private Rectangle rectangle;
    private Grid grid;

    /**
     * Simple graphics position constructor
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(Grid grid){
        super((int) (Math.random() * grid.getCols()), (int) (Math.random() * grid.getRows()), grid);

        this.grid = grid;

        int x = this.grid.columnToX(getCol());
        int y = this.grid.rowToY(getRow());

        this.rectangle = new Rectangle(x, y, this.grid.getCellSize(), this.grid.getCellSize());

        show();
    }

    /**
     * Simple graphics position constructor
     * @param col position column
     * @param row position row
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(int col, int row, Grid grid){
        super(col, row, grid);

        this.grid = grid;

        int x = this.grid.columnToX(col);
        int y = this.grid.rowToY(row);

        this.rectangle = new Rectangle(x, y, this.grid.getCellSize(), this.grid.getCellSize());

        show();
    }

    /**
     * @see GridPosition#show()
     */
    @Override
    public void show() {
        this.rectangle.fill();
    }

    /**
     * @see GridPosition#hide()
     */
    @Override
    public void hide() {
        this.rectangle.delete();
    }

    /**
     * @see GridPosition#moveInDirection(GridDirection, int)
     */
    @Override
    public void moveInDirection(GridDirection direction, int distance) {

        int initialCol = getCol();
        int initialRow = getRow();

        super.moveInDirection(direction, distance);

        int dx = grid.columnToX(getCol()) - grid.columnToX(initialCol);
        int dy = grid.rowToY(getRow()) - grid.rowToY(initialRow);

        this.rectangle.translate(dx,dy);

    }

    /**
     * @see AbstractGridPosition#setColor(GridColor)
     */
    @Override
    public void setColor(GridColor color) {
        this.rectangle.setColor(SimpleGfxColorMapper.getColor(color));
        super.setColor(color);
    }
}

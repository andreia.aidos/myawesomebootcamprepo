package org.academiadecodigo.wizard.Grid;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class KeyBordLogic implements KeyboardHandler {

    private Keyboard keyboard;
    private Cursor cursor;

    public KeyBordLogic(Cursor x) {
        cursor = x;
        init();
    }


    //what happens when you click on key
    public void init() {
       
        keyboard = new Keyboard(this);
        //create keybord event,

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(right);

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(left);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(up);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(down);

        KeyboardEvent paint = new KeyboardEvent();
        paint.setKey(KeyboardEvent.KEY_P);
        paint.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(paint);

        KeyboardEvent clear = new KeyboardEvent();
        clear.setKey(KeyboardEvent.KEY_C);
        clear.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(clear);

        KeyboardEvent removeAll = new KeyboardEvent();
        removeAll.setKey(KeyboardEvent.KEY_R);
        removeAll.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(removeAll);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_RIGHT) {
            cursor.moveRight();
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_LEFT) {
            cursor.moveLeft();
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_UP) {
            cursor.moveUp();
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_DOWN) {
            cursor.moveDown();
        }
        if (keyboardEvent.getKey() == keyboardEvent.KEY_P){
            cursor.paint();
        }
        if (keyboardEvent.getKey() == keyboardEvent.KEY_C){
            cursor.clearOne();
        }
        if (keyboardEvent.getKey() == keyboardEvent.KEY_R){
            cursor.removeAll();
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}

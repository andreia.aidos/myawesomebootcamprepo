package org.academiadecodigo.wizard.Grid;

public class Main {
    public static void main(String[] args) {

        Grid grid = new Grid(20, 22);
        grid.init();
        grid.tinyRectangles();
        Cursor cursor = new Cursor();
        cursor.setRectangle(grid);
        cursor.draw();
        KeyBordLogic kb = new KeyBordLogic(cursor);
    }
}

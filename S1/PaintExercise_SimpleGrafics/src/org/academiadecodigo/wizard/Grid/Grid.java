package org.academiadecodigo.wizard.Grid;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.wizard.Grid.grid.Cell;
import org.academiadecodigo.wizard.Grid.grid.position.GridPosition;
import org.academiadecodigo.wizard.Grid.grid.simplegfx.SimpleGfxGridPosition;

import java.util.ArrayList;

public class Grid implements org.academiadecodigo.wizard.Grid.grid.Grid {

    public static final int PADDING = 10;
    public static final int cellSize = 35;
    private int cols;
    private int rows;
    private int maxCols = cols - 1;
    private int maxRows = rows - 1;
    private Rectangle field;
    
    private ArrayList<Cell> gridCells = new ArrayList<>();


    /**
     * Simple graphics grid constructor with a certain number of rows and columns
     *
     * @param cols number of the columns
     * @param rows number of rows
     */
    public Grid(int cols, int rows) {
        this.cols = cols;
        this.rows = rows;
    }

    /**
     * Initializes the field simple graphics rectangle and draws it
     */
    @Override
    public void init() {
        this.field = new Rectangle(PADDING, PADDING, cols * cellSize, rows * cellSize);
        this.field.draw();

    }

    public void tinyRectangles() {
        for (int i = 0; i < cols; i++) {
            for (int j = 0; j < rows; j++) {
               Cell tiny1 = new Cell(PADDING + cellSize * i, PADDING + cellSize * j, cellSize, cellSize);
               gridCells.add(tiny1);
            }
        }
    }



    public int getCellSize() {
        return cellSize;
    }

    @Override
    public int getCols() {
        return this.cols;
    }

    @Override
    public int getRows() {
        return this.rows;
    }

    public int getWidth() {
        return this.field.getWidth();
    }

    public int getHeight() {
        return this.field.getHeight();
    }

    public int getX() {
        return this.field.getX();
    }

    public int getY() {
        return this.field.getY();
    }



    /**
     * @see org.academiadecodigo.wizard.Grid.grid.Grid#makeGridPosition()
     */
    @Override
    public GridPosition makeGridPosition() {
        return new SimpleGfxGridPosition(this);
    }

    /**
     * @see org.academiadecodigo.wizard.Grid.grid.Grid#makeGridPosition(int, int)
     */
    @Override
    public GridPosition makeGridPosition(int col, int row) {
        return new SimpleGfxGridPosition(col, row, this);
    }

    /**
     * Auxiliary method to compute the y value that corresponds to a specific row
     *
     * @param row index
     * @return y pixel value
     */
    public int rowToY(int row) {
        return PADDING + cellSize * row;
    }

    /**
     * Auxiliary method to compute the x value that corresponds to a specific column
     *
     * @param column index
     * @return x pixel value
     */
    public int columnToX(int column) {
        return PADDING + cellSize * column;
    }

}

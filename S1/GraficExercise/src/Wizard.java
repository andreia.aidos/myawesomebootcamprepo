import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Wizard {

    private Picture wizard = new Picture(10, 10, "/Infected.jpeg");
    public void draw(){
        wizard.draw();
    }

    public void moveRight(){
        //mudar posição do x e do y;
        wizard.translate(20,0);
    }

    public void moveLeft(){
        wizard.translate(-20, 0);
    }
}

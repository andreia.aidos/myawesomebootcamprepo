import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Main {
    public static void main(String[] args) {
        // x0, y0, x(final), y(final)
        Rectangle rectangle = new Rectangle(10, 10, 800, 600);
        rectangle.draw();

        Wizard wizard = new Wizard();

        KeyBordLogic kb = new KeyBordLogic();
        kb.setWizard(wizard);
        wizard.draw();
    }
}

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
public class KeyBordLogic implements KeyboardHandler {

    private Keyboard keyboard;
    private Wizard wizard;

    public KeyBordLogic(){
        init();
    }
    //what happens when you click on key
    public void init(){
        System.out.println("keybord init");
        keyboard = new Keyboard(this);
        //create keybord event,

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(right);

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(left);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        //when the key is pressed something should happen, rigth?
        if(keyboardEvent.getKey()==KeyboardEvent.KEY_RIGHT){
            System.out.println("key was pressed");
            wizard.moveRight();
        }
        if (keyboardEvent.getKey()==KeyboardEvent.KEY_LEFT){
            wizard.moveLeft();
        }
    }

    public void setWizard(Wizard wizard) {
        this.wizard = wizard;
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}

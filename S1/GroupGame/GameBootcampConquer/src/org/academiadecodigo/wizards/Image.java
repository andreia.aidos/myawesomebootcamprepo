package org.academiadecodigo.wizards;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Image {

private Picture start = new Picture(10, 0, "resources/Start.jpg");

    public Image() {
        start.draw();
    }

   public void deletePicture(){
        start.delete();
   }
}

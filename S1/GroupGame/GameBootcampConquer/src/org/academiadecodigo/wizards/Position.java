package org.academiadecodigo.wizards;

import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC.Andreia;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Grid.Grid;

import static org.academiadecodigo.wizards.Grid.Grid.CELL_SIZE;
import static org.academiadecodigo.wizards.Grid.Grid.PADDING;

public class Position {

    private double posX;
    private double posY; //it will begin out of canvas ex y=-10

    private GameObjects gameObjects;

    public Position(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }



}
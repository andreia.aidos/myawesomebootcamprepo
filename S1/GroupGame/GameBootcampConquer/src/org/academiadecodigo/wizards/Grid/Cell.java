package org.academiadecodigo.wizards.Grid;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import static org.academiadecodigo.wizards.Grid.Grid.CELL_SIZE;
import static org.academiadecodigo.wizards.Grid.Grid.PADDING;

public class Cell {
    int row;
    int col;
    Rectangle rectangle;

    public Cell(int row, int col) {
        this.row = row;
        this.col = col;
        rectangle = new Rectangle(col * CELL_SIZE + PADDING, row * CELL_SIZE + PADDING, CELL_SIZE, CELL_SIZE);
        rectangle.draw();
        rectangle.setColor(Color.WHITE);
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

}

package org.academiadecodigo.wizards.Grid;

public class Grid {

    public static final int CELL_SIZE = 100;

    public static final int PADDING = 10;
    private int rows;
    private int cols;

    private Cell[][] cells;

    public Grid(int rows, int cols) {

        this.rows = rows;
        this.cols = cols;

        cells = new Cell[rows][cols];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                cells[row][col] = new Cell(row, col);
            }
        }
    }

    public Cell getCell(int row, int col) {
        return cells[row][col];
    }

    public int getRows(int y) {
        return rows;
    }

    public int getCols(int x) {
        return cols;
    }

    public int XtoCol(double x) {
        if (x >= PADDING && x < CELL_SIZE + PADDING) {
            return 1;
        }
        if (x >= CELL_SIZE + PADDING && x < CELL_SIZE * 2) {
            return 2;
        }
        if (x >= CELL_SIZE + PADDING && x < CELL_SIZE * 3) {
            return 3;
        }if (x >= CELL_SIZE + PADDING && x < CELL_SIZE * 4) {
            return 4;
        }if (x >= CELL_SIZE + PADDING && x < CELL_SIZE * 5) {
            return 5;
        }if (x >= CELL_SIZE + PADDING && x < CELL_SIZE * 6) {
            return 6;
        }if (x >= CELL_SIZE + PADDING && x < CELL_SIZE * 7) {
            return 7;
        }
        if (x >= CELL_SIZE + PADDING && x < CELL_SIZE * 8) {
            return 8;
        }
        if (x >= CELL_SIZE + PADDING && x < CELL_SIZE * 9) {
            return 9;
        }
        if (x >= CELL_SIZE + PADDING && x <= CELL_SIZE * 10) {
            return 10;
        }
        return -1;
    }
    public int YtoRow(double y) {
        if (y >= PADDING && y < CELL_SIZE + PADDING) {
            return 1;
        }
        if (y >= CELL_SIZE + PADDING && y < CELL_SIZE * 2) {
            return 2;
        }
        if (y >= CELL_SIZE + PADDING && y < CELL_SIZE * 3) {
            return 3;
        }if (y >= CELL_SIZE + PADDING && y < CELL_SIZE * 4) {
            return 4;
        }if (y >= CELL_SIZE + PADDING && y < CELL_SIZE * 5) {
            return 5;
        }if (y >= CELL_SIZE + PADDING && y < CELL_SIZE * 6) {
            return 6;
        }if (y >= CELL_SIZE + PADDING && y < CELL_SIZE * 7) {
            return 7;
        }
        if (y >= CELL_SIZE + PADDING && y < CELL_SIZE * 8) {
            return 8;
        }
        if (y >= CELL_SIZE + PADDING && y <= CELL_SIZE * 9) {
            return 9;
        }
        return -1;
    }

}

package org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.AffectsMotivation;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Position;

public class Mariana extends AffectsMotivation {

    private Position pos = new Position(Math.random()*randomWidth, initY);

    private Picture mariana;
    public Mariana() throws InterruptedException {
        mariana = new Picture (pos.getPosX(), pos.getPosY(), "resources/Mariana.png");
        mariana.draw();
        move();
    }

    @Override
    public void affect(){
        if(wizard.getMotivation()< wizard.getMAX_MOTIVATION()){
            wizard.setMotivation(wizard.getMotivation()+2);
        }
    }

    //TODO: change probability; mariana só aparece quando a motivação está no zero
    public GameObjects probability() throws InterruptedException {
        int probability = (int) (Math.random() * 11);
        if (probability <= 2) {
            return new Mariana();
        }
        return null;
    }

    public void move() throws InterruptedException {
        while (mariana.getY() < endOfMove) {
            Thread.sleep(delay);
            mariana.translate(0, translateY);
        }
        if (mariana.getY() == endOfMove){
            mariana.delete();
        }
    }
}
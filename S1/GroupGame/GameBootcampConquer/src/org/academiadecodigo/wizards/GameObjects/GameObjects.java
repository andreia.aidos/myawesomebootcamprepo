package org.academiadecodigo.wizards.GameObjects;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.Grid.Grid;
import org.academiadecodigo.wizards.Position;
import org.academiadecodigo.wizards.WizardOfOS;

public abstract class GameObjects {

    protected Position position;
    protected int randomWidth = 600;
    protected int initY = -200;
    protected Picture picture;
    protected int delay = 2;
    protected int endOfMove = 1000;
    protected int translateY = 1;
    protected WizardOfOS wizard;
    protected boolean crashed = false;
    protected Grid grid;

    public int getInitY() {
        return initY;
    }
    public abstract void affect();

    public boolean isCrashed() {
        return crashed;
    }

    public boolean setCrashed(boolean crashed) {
        this.crashed = crashed;
        return crashed;
    }

    public abstract GameObjects probability() throws InterruptedException;
}

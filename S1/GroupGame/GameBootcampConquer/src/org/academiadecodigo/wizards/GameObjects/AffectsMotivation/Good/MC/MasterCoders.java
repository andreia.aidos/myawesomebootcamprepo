package org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.AffectsMotivation;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Position;

public abstract class MasterCoders extends AffectsMotivation {

    @Override
    public void affect() {
        if (wizard.getMotivation() < wizard.getMAX_MOTIVATION()) {
            wizard.setMotivation(wizard.getMotivation() + 1);
        }
    }
}




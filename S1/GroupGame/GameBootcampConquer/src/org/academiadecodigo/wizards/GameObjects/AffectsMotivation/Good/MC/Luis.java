package org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Position;

public class Luis extends MasterCoders{

    private Position pos = new Position((int) (Math.random()*randomWidth), (int) initY);
    private Picture luis;
    public Luis() throws InterruptedException {
        luis = new Picture(pos.getPosX(), pos.getPosY(), "resources/Luis.png");
        luis.draw();
        move();
    }

    public Position getPos() {
        return pos;
    }

    // TODO: insert correct picture and initial position;


    //TODO: change probability;
    public GameObjects probability() throws InterruptedException {
        int probability = (int) (Math.random() * 9);
        if (probability <= 2) {
            return new Luis();
        }
        return null;
    }

    public void move() throws InterruptedException {
        while (luis.getY() < endOfMove) {
            Thread.sleep(delay);
            luis.translate(0, translateY);
        }
        if (luis.getY() == endOfMove){
            luis.delete();
        }
    }
}

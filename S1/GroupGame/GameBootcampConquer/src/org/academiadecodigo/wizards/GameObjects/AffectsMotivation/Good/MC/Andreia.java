package org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Grid.Grid;
import org.academiadecodigo.wizards.Position;

public class Andreia extends MasterCoders {

    private Position pos;
    private Picture andreia;

    public Andreia(Grid grid) throws InterruptedException {
        this.grid = grid;
        andreia = new Picture(110, 110, "resources/Andreia.png");
        andreia.draw();
        pos = new Position(this.grid.XtoCol(andreia.getX()), this.grid.YtoRow(andreia.getY()));
        move();
    }

    public Position getPos() {
        return pos;
    }

    public Picture getAndreia() {
        return andreia;
    }

    //TODO: change probability;
    public GameObjects probability() throws InterruptedException {
        int probability = (int) (Math.random() * 9);
        if (probability <= 2) {
            return new Andreia(grid);
        }
        return null;
    }

    public void move() throws InterruptedException {
        while (andreia.getY() < endOfMove) {
            Thread.sleep(delay);
            andreia.translate(0, translateY);
            pos.setPosY(grid.YtoRow(andreia.getY() + translateY));
            if (andreia.getY() == endOfMove) {
                andreia.delete();
            }
        }
    }
}

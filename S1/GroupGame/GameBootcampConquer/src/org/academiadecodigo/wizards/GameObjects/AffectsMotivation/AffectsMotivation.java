package org.academiadecodigo.wizards.GameObjects.AffectsMotivation;

import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.WizardOfOS;

public class AffectsMotivation extends GameObjects {

    protected WizardOfOS wizard;

    @Override
    public void affect() {
        if (wizard.getMotivation() < wizard.getMAX_MOTIVATION()) {
            wizard.setMotivation(wizard.getMotivation() + 1);
        }
        if (wizard.getMotivation() > wizard.getMIN_MOTIVATION()) {
            wizard.setMotivation(wizard.getMotivation() - 1);
        }
    }

    @Override
    public GameObjects probability() throws InterruptedException {
        return null;
    }
}

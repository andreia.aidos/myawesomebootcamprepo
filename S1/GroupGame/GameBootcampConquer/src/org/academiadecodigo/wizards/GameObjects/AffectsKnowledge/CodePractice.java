package org.academiadecodigo.wizards.GameObjects.AffectsKnowledge;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Position;

public class CodePractice extends AffectsKnowledge {

    private Position pos = new Position(Math.random()*randomWidth, initY);
    private Picture codePractice;
    public CodePractice() throws InterruptedException {
        codePractice = new Picture (pos.getPosX(), pos.getPosY(), "resources/CodePractice.png");
        codePractice.draw();
        move();
    }

    @Override
    public void affect() {
        if (wizard.getKnowledge() < wizard.getMAX_KNOWLEDGE()) {
            wizard.setKnowledge(wizard.getKnowledge() + 2);
        }
    }

    // TODO: insert correct picture and initial position;



    //TODO: change probability;
    public GameObjects probability() throws InterruptedException {
        int probability = (int) (Math.random() * 11);
        if (probability <= 2) {
            return new CodePractice();
        }
        return null;
    }

    public void move() throws InterruptedException {
        while (codePractice.getY() < endOfMove) {
            Thread.sleep(delay);
            codePractice.translate(0, translateY);
        }
        if (codePractice.getY() == endOfMove){
            codePractice.delete();
        }
    }
}
package org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Position;

public class Mike extends MasterCoders {

    private Position pos = new Position(Math.random() * randomWidth, initY);
    private Picture mike;

    public Mike() throws InterruptedException {
        mike = new Picture(pos.getPosX(), pos.getPosY(), "resources/Mike.png");
        mike.draw();
        move();
    }

    // TODO: insert correct picture and initial position;

    public Position getPos() {
        return pos;
    }

    //TODO: change probability;
    public GameObjects probability() throws InterruptedException {
        int probability = (int) (Math.random() * 9);
        if (probability <= 2) {
            return new Mike();
        }
        return null;
    }

    public void move() throws InterruptedException {
        while (mike.getY() < endOfMove) {
            Thread.sleep(delay);
            mike.translate(0, translateY);
        }
        if (mike.getY() == endOfMove) {
            mike.delete();
        }
    }
}

package org.academiadecodigo.wizards.GameObjects.AffectsKnowledge;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Position;

public class Lecture extends AffectsKnowledge{

    private Position pos = new Position(Math.random()*randomWidth, initY);
    private Picture lecture;

    public Lecture() throws InterruptedException {
        lecture = new Picture (pos.getPosX(), pos.getPosY(), "resources/Lecture.png");
        lecture.draw();
        move();
    }

    //TODO: change probability;
    public GameObjects probability() throws InterruptedException {
        int probability = (int) (Math.random() * 11);
        if (probability <= 6) {
            return new Lecture();
        }
        return null;
    }

    public void move() throws InterruptedException {
        while (lecture.getY() < endOfMove) {
            Thread.sleep(delay);
            lecture.translate(0, translateY);
        }
        if (lecture.getY() == endOfMove){
            lecture.delete();
        }
    }
}

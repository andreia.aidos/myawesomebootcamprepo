package org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Bad;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.AffectsMotivation;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Position;

public class HardCode extends AffectsMotivation {

    private Position pos = new Position((int) (Math.random()*randomWidth), (int) initY);
    private Picture hardCode;
    public HardCode() throws InterruptedException {
        hardCode = new Picture (pos.getPosX(), pos.getPosY(), "resources/HardCode.png");
        hardCode.draw();
        move();
    }

    @Override
    public void affect(){
        if (wizard.getMotivation()> wizard.getMIN_MOTIVATION()) {
            wizard.setMotivation(wizard.getMotivation()-1);
        }
    }

    //TODO: change probability;
    public GameObjects probability() throws InterruptedException {
        int probability = (int) (Math.random() * 11);
        if (probability <= 5) {
            return new HardCode();
        }
        return null;
    }

    public void move() throws InterruptedException {
        while (hardCode.getY() < endOfMove) {
            Thread.sleep(delay);
            hardCode.translate(0, translateY);
        }
        if (hardCode.getY() == endOfMove){
            hardCode.delete();
        }
    }
}

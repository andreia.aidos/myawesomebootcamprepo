package org.academiadecodigo.wizards.GameObjects.AffectsKnowledge;

import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.WizardOfOS;

public class AffectsKnowledge extends GameObjects {

    protected WizardOfOS wizard;

    @Override
    public void affect() {
        if (wizard.getKnowledge() < wizard.getMAX_KNOWLEDGE()) {
            wizard.setKnowledge(wizard.getKnowledge() + 1);
        }
    }

    @Override
    public GameObjects probability() throws InterruptedException {
        return null;
    }
}

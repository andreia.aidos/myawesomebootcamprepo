package org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Position;

public class Diogo extends MasterCoders {

    private Position pos;
    private Picture diogo;

    public Diogo() throws InterruptedException {
        diogo = new Picture(20, 5, "resources/Diogo.png");
        diogo.draw();
        pos = new Position(diogo.getX(), diogo.getY());
        move();
    }

    //TODO: change probability;
    public GameObjects probability() throws InterruptedException{
        int probability = (int) (Math.random() * 9);
        if (probability <= 2) {
            return new Diogo();
        }
        return null;
    }

    public void move() throws InterruptedException {
        while (diogo.getY() < endOfMove) {
            Thread.sleep(delay);
            diogo.translate(0, translateY);
            pos.setPosY(pos.getPosY() + translateY);
            if (diogo.getY() == endOfMove) {
                diogo.delete();
            }
        }
    }

// atualizar a posição cada vez que o objeto mexe, verificar se tem o mesmo x e y

    //TODO: fiquei aqui!!!
    public void crash(){
        if(pos.getPosY()+200 == (wizard.getPos().getPosY())){
            diogo.delete();
        }
    }
}

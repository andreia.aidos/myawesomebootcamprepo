package org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Bad;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.AffectsMotivation;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Position;

public class Weed extends AffectsMotivation {

    private Position pos = new Position((int) (Math.random()*randomWidth), (int) initY);
    private Picture weed;
    public Weed() throws InterruptedException {
        weed = new Picture (pos.getPosX(), pos.getPosY(), "resources/Weed.png");
        weed.draw();
        weed.grow(-30, -30);
        move();
    }

    @Override
    public void affect(){
        if (wizard.getMotivation()> wizard.getMIN_MOTIVATION()) {
            wizard.setMotivation(wizard.getMotivation()-2);
        }
    }

    //TODO: change probability;
    public GameObjects probability() throws InterruptedException {
        int probability = (int) (Math.random() * 11);
        if (probability <= 5) {
            return new Weed();
        }
        return null;
    }

    public void move() throws InterruptedException {
        while (weed.getY() < endOfMove) {
            Thread.sleep(delay);
            weed.translate(0, translateY);
        }
        if (weed.getY() == endOfMove){
            weed.delete();
        }
    }
}

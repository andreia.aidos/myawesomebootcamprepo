package org.academiadecodigo.wizards;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.Grid.Grid;

public class WizardOfOS {

    private int knowledge = 0;
    private final int MAX_KNOWLEDGE = 20;
    private int motivation = 20;
    private final int MAX_MOTIVATION = 20;
    private final int MIN_MOTIVATION = 0;
    private Position pos;

    private Grid grid;

    private Picture wizardPic = new Picture(110, 750, "resources/Cadet.png");
    public WizardOfOS(Grid grid){
        this.grid = grid;
        wizardPic.draw();
        pos = new Position(this.grid.XtoCol(wizardPic.getX()), this.grid.YtoRow(wizardPic.getY()));

    }
    public int getKnowledge() {
        return knowledge;
    }

    public int getMotivation() {
        return motivation;
    }

    public int getMAX_KNOWLEDGE() {
        return MAX_KNOWLEDGE;
    }

    public int getMAX_MOTIVATION() {
        return MAX_MOTIVATION;
    }

    public int getMIN_MOTIVATION() {
        return MIN_MOTIVATION;
    }

    public void setKnowledge(int knowledge) {
        this.knowledge = knowledge;
    }

    public void setMotivation(int motivation) {
        this.motivation = motivation;
    }

    public Position getPos() {
        return pos;
    }


    //TODO: make limit flexible
    public void moveRight() {
        if (wizardPic.getMaxX() < 900) {
            wizardPic.translate(10, 0);
            pos.setPosX(grid.XtoCol(wizardPic.getX()+10));
        }
    }

    //TODO: make limit flexible
    public void moveLeft() {
        if (wizardPic.getX() > 20) {
            wizardPic.translate(-10, 0);
            pos.setPosX(grid.XtoCol(wizardPic.getX()-10));
        }
    }
}

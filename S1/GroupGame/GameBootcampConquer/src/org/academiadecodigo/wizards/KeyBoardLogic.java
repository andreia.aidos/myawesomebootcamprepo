package org.academiadecodigo.wizards;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.awt.*;

public class KeyBoardLogic implements KeyboardHandler {

    private Keyboard keyboard;
    private WizardOfOS wizardOfOS;

    private Image image;

    private Game game;

    /* keyBordLogic for: restart game when game over and initiate the game after explanation!
     * */
    //what happens when you click on key
    public void init() {
        keyboard = new Keyboard(this);
        //create keyboard event,

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(right);

        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(left);

        KeyboardEvent startGame = new KeyboardEvent();
        startGame.setKey(KeyboardEvent.KEY_SPACE);
        startGame.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(startGame);

    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        //when the key is pressed something should happen, rigth?
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_RIGHT) {
            wizardOfOS.moveRight();
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_LEFT) {
            wizardOfOS.moveLeft();
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
            Picture k1 = new Picture(910, 500, "resources/Knowledge7.png"); //TODO: change this
            k1.draw();
        }
    }

    public void setWizard(WizardOfOS wizardOfOS) {
        this.wizardOfOS = wizardOfOS;
    }

    public void setImage(Image image) { this.image = image;}

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
}

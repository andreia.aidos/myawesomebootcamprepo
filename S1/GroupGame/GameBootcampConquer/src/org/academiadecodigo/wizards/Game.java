package org.academiadecodigo.wizards;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.wizards.GameObjects.AffectsKnowledge.CodePractice;
import org.academiadecodigo.wizards.GameObjects.AffectsKnowledge.Lecture;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Bad.HardCode;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Bad.Weed;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC.Andreia;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC.Diogo;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC.Luis;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.MC.Mike;
import org.academiadecodigo.wizards.GameObjects.AffectsMotivation.Good.Mariana;
import org.academiadecodigo.wizards.GameObjects.GameObjects;
import org.academiadecodigo.wizards.Grid.Grid;

public class Game {

    private GameObjects gameObjects;
    private Luis luis;
    private Andreia andreia;
    private Grid grid;
    private Mike mike;
    private Diogo diogo;
    private WizardOfOS wizard;
    private KeyBoardLogic keyBoardLogic;
    private Weed weed;
    private Lecture lecture;
    private HardCode hardCode;
    private CodePractice codePractice;
    private Picture knowledge;
    private Mariana mariana;

    public Game() throws InterruptedException {
        Rectangle rectangle = new Rectangle(10, 10, 1000, 930);
        rectangle.draw();
        rectangle.setColor(Color.WHITE);
        grid = new Grid(9, 10);
        background();
        this.knowledge = new Picture(910, 500, "resources/Knowledge1.png");
        Picture motivation = new Picture(900, 20, "resources/Motivation.png");
        motivation.draw();
        Picture motivation1 = new Picture(900, 120, "resources/Motivation.png");
        motivation1.draw();
        Picture motivation2 = new Picture(900, 220, "resources/Motivation.png");
        motivation2.draw();
        Picture motivation3 = new Picture(900, 320, "resources/Motivation.png");
        motivation3.draw();
        knowledge.draw();
        this.wizard = new WizardOfOS(grid);
        keyBoardLogic = new KeyBoardLogic();
        keyBoardLogic.setWizard(wizard);
        keyBoardLogic.init();
        this.lecture = new Lecture();
        this.codePractice = new CodePractice();
        this.hardCode = new HardCode();
        this.luis = new Luis();
        this.andreia = new Andreia(grid);
        this.mike = new Mike();
        this.diogo = new Diogo();
        this.mariana = new Mariana();
        this.weed = new Weed();
    }

    public boolean crash() throws InterruptedException {
        if (luis.getPos().equals(wizard.getPos())) {
            return luis.setCrashed(true);
        }
        return false;
    }

    public void background() {
        Picture background = new Picture(10, 0, "resources/Background.jpg");
        background.draw();
    }
}





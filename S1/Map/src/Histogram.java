import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//key is teh String, number of occurrences is the value
public class Histogram implements Iterable<String> {

    private Map<String, Integer> map = new HashMap<>();
    private String[] arrayStrings;

    public Histogram(String string) {
        arrayStrings = string.split(" ");
        buildMap();
    }

    public void buildMap() {
        for (String string : arrayStrings) {
            if (map.containsKey(string)) {
                map.put(string, map.get(string) + 1);
                continue;
            }
            map.put(string, 1);
        }
    }

    public Integer getValue(String key) {
        return map.get(key);
    }

    public int size() {
        return map.keySet().size();
    }

    public Iterator<String> iterator() {
        return map.keySet().iterator();
    }
}

import java.util.HashMap;
import java.util.Iterator;

public class InheritanceHistogram extends HashMap<String, Integer> implements Iterable<String> {

    private String[] arrayStrings;

    public InheritanceHistogram(String string) {
        arrayStrings = string.split(" ");
        buildMap();
    }

    public void buildMap() {
        for (String string : arrayStrings) {
            if (containsKey(string)) {
                put(string, get(string) + 1);
                continue;
            }
            put(string, 1);
        }
    }

    public int size() {
        return keySet().size();
    }

    public Iterator<String> iterator() {
        return keySet().iterator();
    }
}

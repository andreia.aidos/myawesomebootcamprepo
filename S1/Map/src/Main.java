import java.util.Iterator;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        String str = "Hé pá uma uma uma coisa coisa qualquer";
        Histogram h1 = new Histogram(str);
        System.out.println("COMPOSITION");
        System.out.println("MAP has " + h1.size() + " dif words!");
        for (String key : h1) {
            System.out.println(key + " : " + h1.getValue(key));
        }


        System.out.println("-----------------------------");
        System.out.println("INHERITANCE");
        InheritanceHistogram h2 = new InheritanceHistogram(str);
        System.out.println("MAP has " + h2.size() + " dif words!");
        for (String key : h2) {
            System.out.println(key + " : " + h2.get(key));
        }
    }
}
var ajax;

if (window.XMLHttpRequest) {
	ajax = new XMLHttpRequest();
} else if (window.ActiveXObject) {
	ajax = new ActiveXObject("Microsoft.XMLHTTP");
}

ajax.onreadystatechange = function () {
	if (ajax.readyState === 4 && ajax.status === 200) {
		customerData = JSON.parse(ajax.responseText);
		populateTable();
	}
};

ajax.open("GET", "http://localhost:8080/javabank5/api/customer", true);
ajax.send();

var customerTable = document.getElementById("customer-table");

function populateTable() {
	customerData.forEach((customer) => {
		row = customerTable.insertRow(-1);

		cell1 = document.createElement("td");
		cellText1 = document.createTextNode(customer.firstName);
		cell1.appendChild(cellText1);

		cell2 = document.createElement("td");
		cellText2 = document.createTextNode(customer.lastName);
		cell2.appendChild(cellText2);

		cell3 = document.createElement("td");
		cellText3 = document.createTextNode(customer.email);
		cell3.appendChild(cellText3);

		cell4 = document.createElement("td");
		cellText4 = document.createTextNode(customer.phone);
		cell4.appendChild(cellText4);

		cell5 = document.createElement("td");
		cellBtn1 = document.createElement("button");
		cellText5 = document.createTextNode("Edit");
		cellBtn1.classList.add("btn");
		cellBtn1.classList.add("btn-success");
		cellBtn1.appendChild(cellText5);
		cell5.appendChild(cellBtn1);

		cell6 = document.createElement("td");
		cellBtn2 = document.createElement("button");
		cellText6 = document.createTextNode("Delete");
		cellBtn2.classList.add("btn");
		cellBtn2.classList.add("btn-danger");
		cellBtn2.appendChild(cellText6);
		cell6.appendChild(cellBtn2);

		row.appendChild(cell1);
		row.appendChild(cell2);
		row.appendChild(cell3);
		row.appendChild(cell4);
		row.appendChild(cell5);
		row.appendChild(cell6);
	});
}

$(document).ready(function() {
    getCustomers();
	$('#customer-form').submit(function(event) {
		event.preventDefault(); 
		var formData = $('#myForm').serializeArray();
		var jsonData = JSON.stringify(formData);
		console.log(jsonData);
	  });
});

function errorCallback(request, status, error) {
    console.log("Ajax error: " + status + ", " + error);
};

function getCustomers() {
$.ajax({
    url: 'http://localhost:8080/javabank5/api/customer',
	dataType: 'json',
    async: true,
    success: populateTable,
    error: errorCallback
});
};

function addCustomer() {
	$.ajax({
		url: 'http://localhost:8080/javabank5/api/customer',
		type: 'POST',
		dataType: 'json',
		async: true,
		success: customerInfo,
		error: errorCallback
	});
}

function customerInfo() {

}

var customerTable = $("#customer-table");

function populateTable(customerData) {
	$.each(customerData, function(index, customer){
		var row = $("<tr></tr>");

		$("<td></td>").text(customer.firstName).appendTo(row);
		$("<td></td>").text(customer.lastName).appendTo(row);
		$("<td></td>").text(customer.email).appendTo(row); 
		$("<td></td>").text(customer.phone).appendTo(row);

		var editBtn = $("<button>Edit</button>").addClass("btn btn-success");
        var deleteBtn = $("<button>Delete</button>").addClass("btn btn-danger");
        $("<td></td>").append(editBtn).appendTo(row);
        $("<td></td>").append(deleteBtn).appendTo(row);
        customerTable.append(row);
	});
};
